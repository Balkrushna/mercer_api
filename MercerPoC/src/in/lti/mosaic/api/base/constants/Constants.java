package in.lti.mosaic.api.base.constants;

/**
 * 
 * @author rushi
 *
 */
public class Constants {

  public static class ValidationTypes {
    public static final String L1 = "L1";
    //public static final String L2 = "L2";
    public static final String L3A = "L3a";
    public static final String L3B = "L3b";
  }
  
  public static class RequestStatus{
    public static final String REQUESTRECEIVED = "1-Request-received";
    public static final String ADDEDTOQUEUE = "2-Added-to-queue";
    public static final String PICKEDUPFORPROCESSING = "3-Picked-up-for-processing";
    public static final String INPUTDOCUMENTDOWNLOADED = "4-Input-document-downloaded";
    public static final String INPUTDOCUMENTDOWNLOADEDERROR = "4-Input-document-download-error";
    public static final String PROCESSINGFINISHED = "5-Processing-finished";
    public static final String PROCESSINGERROR = "5-Processing-error";
    public static final String OUTPUTDOCUMENTCREATED = "6-Output-document-created";
    public static final String OUTPUTDOCUMENTCREATEDERROR = "6-Output-document-create-error";
    public static final String DELIVEREDSUCCESSFULLY = "7-Delivered-successfully";
    public static final String DEADLETTER = "7-Dead-letter";
    
    public static final String ERROR = "ERROR";
  }
  
  public static class StatusConstants {
    public static final String ID = "id";
    public static final String REQUESTID = "request_id";
    public static final String DOCUMENTID = "document_id";
    public static final String STATUS = "status";
    public static final String ENVIRONMENTNAME = "environment_name";
    public static final String REMARKS = "remarks";
  }
  
  public static class Request {
    public static final String REQUESTID = "requestId";
    public static final String PRIORITY = "priority";
    public static final String VALIDATIONTYPE = "validationType";
    public static final String DOCUMENTID = "documentId";
    public static final String ENVIRONMENTNAME = "environmentName";
    public static final String STATUS = StatusConstants.STATUS;
    public static final String MESSAGE = "message";
    public static final String MOSAIC_PROCESSING_FINISHED = "Mosaic processing finished";
    public static final String MOSAIC_PROCESSING_FAILED = "Mosaic processing failed";
    public static final String MOSAIC_DOC_DOWNLOAD_FAILED = "Document download failed for Mosaic";
    public static final String MOSAIC_DOC_UPLOAD_FAILED = "Document upload failed for Mosaic";
  }
  
  public static class ResponseStatus {
    public static final Integer HTTPSTATUS_200 = 200;
    public static final Integer HTTPSTATUS_400 = 400;
    public static final Integer HTTPSTATUS_500 = 500;
  }

}
