package in.lnt.validations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.time.DateUtils;

import in.lnt.enums.TransformationTypes;

public class TransformationCreator {

	static String[] dateFormatList = new String[] { "MM/dd/yyyy", "dd/MM/yyyy", "yyyy-dd-MM", "yyyy-MM-dd" };

	static public void transformationsCreator(String key, String tranformationType, HashMap<String, String> data) {
		if (EnumUtils.isValidEnum(TransformationTypes.class, tranformationType)) {

			switch (TransformationTypes.valueOf(tranformationType.toUpperCase())) {
			case EXTRACTYEAR:
				dateTransformation(data, key, "yyyy");
				break;
			case EXTRACTDAY_NUMERIC:
				dateTransformation(data, key, "DD");
				break;
			case EXTRACTDAY_TEXT:
				dateTransformation(data, key, "EEEE");
				break;
			case EXTRACTMONTH:
				dateTransformation(data, key, "MM");
				break;
			}
		}
	}

	static void dateTransformation(HashMap<String, String> data, String key, String dateFormat) {
		try {
			Date date = DateUtils.parseDateStrictly(data.get(key), dateFormatList);
			data.put(key, String.valueOf(new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(date)));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
