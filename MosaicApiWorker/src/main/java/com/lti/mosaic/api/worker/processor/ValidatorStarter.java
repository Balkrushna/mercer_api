package com.lti.mosaic.api.worker.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;

/**
 * @author rushikesh
 *
 */
public class ValidatorStarter {
	
	/**
	 * @author Akhil 
	 */
	private ValidatorStarter() {
		//constructor
	}
  
  private static Logger logger = LoggerFactory.getLogger(ValidatorStarter.class);
  
  public static void start(BaseValidator validationBase)
  {
      logger.debug(LoggerConstants.LOG_MAXIQAPI, " >> start() ");

      new Thread(validationBase).start();
      
      logger.debug(LoggerConstants.LOG_MAXIQAPI, " << start() ");
  }

}