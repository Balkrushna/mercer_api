// Generated from Expression.g4 by ANTLR 4.4

	package com.lti.mosaic.antlr4.parser;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link ExpressionListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class ExpressionBaseListener implements ExpressionListener {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignmentExpr( ExpressionParser.AssignmentExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignmentExpr( ExpressionParser.AssignmentExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWhileStatExpr( ExpressionParser.WhileStatExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWhileStatExpr( ExpressionParser.WhileStatExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBooleanAtom( ExpressionParser.BooleanAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBooleanAtom( ExpressionParser.BooleanAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAdditiveExpr( ExpressionParser.AdditiveExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAdditiveExpr( ExpressionParser.AdditiveExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionExpr( ExpressionParser.FunctionExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionExpr( ExpressionParser.FunctionExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParExpr( ExpressionParser.ParExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParExpr( ExpressionParser.ParExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnaryMinusExpr( ExpressionParser.UnaryMinusExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnaryMinusExpr( ExpressionParser.UnaryMinusExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBlock( ExpressionParser.BlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBlock( ExpressionParser.BlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWhileStat( ExpressionParser.WhileStatContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWhileStat( ExpressionParser.WhileStatContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayValues( ExpressionParser.ArrayValuesContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayValues( ExpressionParser.ArrayValuesContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNParametersFunctions( ExpressionParser.NParametersFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNParametersFunctions( ExpressionParser.NParametersFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParamN( ExpressionParser.FunctionParamNContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParamN( ExpressionParser.FunctionParamNContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConditionBlock( ExpressionParser.ConditionBlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConditionBlock( ExpressionParser.ConditionBlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOrExpr( ExpressionParser.OrExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOrExpr( ExpressionParser.OrExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNilAtom(ExpressionParser.NilAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNilAtom( ExpressionParser.NilAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelationalExpr( ExpressionParser.RelationalExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelationalExpr( ExpressionParser.RelationalExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterList( ExpressionParser.ListContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitList( ExpressionParser.ListContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNumberAtom( ExpressionParser.NumberAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNumberAtom( ExpressionParser.NumberAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterTwoParamterFunctions( ExpressionParser.TwoParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitTwoParamterFunctions( ExpressionParser.TwoParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNotExpr( ExpressionParser.NotExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNotExpr( ExpressionParser.NotExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStringAtom( ExpressionParser.StringAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStringAtom( ExpressionParser.StringAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicationExpr( ExpressionParser.MultiplicationExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicationExpr( ExpressionParser.MultiplicationExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatementExpr( ExpressionParser.StatementExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatementExpr( ExpressionParser.StatementExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPowExpr( ExpressionParser.PowExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPowExpr( ExpressionParser.PowExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEqualityExpr( ExpressionParser.EqualityExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEqualityExpr( ExpressionParser.EqualityExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam4( ExpressionParser.FunctionParam4Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam4( ExpressionParser.FunctionParam4Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam3( ExpressionParser.FunctionParam3Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam3( ExpressionParser.FunctionParam3Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIdAtom( ExpressionParser.IdAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIdAtom( ExpressionParser.IdAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam2( ExpressionParser.FunctionParam2Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam2( ExpressionParser.FunctionParam2Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayAtom( ExpressionParser.ArrayAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayAtom( ExpressionParser.ArrayAtomContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam1( ExpressionParser.FunctionParam1Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam1( ExpressionParser.FunctionParam1Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam0( ExpressionParser.FunctionParam0Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam0( ExpressionParser.FunctionParam0Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAtomExpr( ExpressionParser.AtomExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAtomExpr( ExpressionParser.AtomExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatBlock( ExpressionParser.StatBlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatBlock( ExpressionParser.StatBlockContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayElementTypes( ExpressionParser.ArrayElementTypesContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayElementTypes( ExpressionParser.ArrayElementTypesContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFourParamterFunctions( ExpressionParser.FourParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFourParamterFunctions( ExpressionParser.FourParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSingleParamterFunctions( ExpressionParser.SingleParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSingleParamterFunctions( ExpressionParser.SingleParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOtherExpr( ExpressionParser.OtherExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOtherExpr( ExpressionParser.OtherExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatement( ExpressionParser.StatementContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatement( ExpressionParser.StatementContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArgumentsN( ExpressionParser.ArgumentsNContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArgumentsN( ExpressionParser.ArgumentsNContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIfStat( ExpressionParser.IfStatContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIfStat( ExpressionParser.IfStatContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignment( ExpressionParser.AssignmentContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignment( ExpressionParser.AssignmentContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIfStatExpr( ExpressionParser.IfStatExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIfStatExpr( ExpressionParser.IfStatExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParse( ExpressionParser.ParseContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParse( ExpressionParser.ParseContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments1( ExpressionParser.Arguments1Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments1( ExpressionParser.Arguments1Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments4( ExpressionParser.Arguments4Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments4( ExpressionParser.Arguments4Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterZeroParamterFunctions( ExpressionParser.ZeroParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitZeroParamterFunctions( ExpressionParser.ZeroParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments2( ExpressionParser.Arguments2Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments2( ExpressionParser.Arguments2Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAndExpr( ExpressionParser.AndExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAndExpr( ExpressionParser.AndExprContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterThreeParamterFunctions( ExpressionParser.ThreeParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitThreeParamterFunctions( ExpressionParser.ThreeParamterFunctionsContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments3( ExpressionParser.Arguments3Context ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments3( ExpressionParser.Arguments3Context ctx) {// default implementation ignored 
    }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule( ParserRuleContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule( ParserRuleContext ctx) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal( TerminalNode node) {// default implementation ignored 
    }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode( ErrorNode node) {// default implementation ignored 
    }
}